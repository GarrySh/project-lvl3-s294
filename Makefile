install:
	yarn install

lint:
	yarn run eslint .

test:
	yarn run test

test.watch:
	yarn run test --watch

build:
	rm -rf dist
	yarn run build

dev:
	rm -rf dist
	yarn run dev

dev.watch:
	yarn webpack-dev-server --open

clear:
	rm -rf dist
	rm -rf node_modules
