import autoprefixer from 'autoprefixer';
import path from 'path';
import precss from 'precss';
import HtmlWebpackPlugin from 'html-webpack-plugin';

export default () => ({
  mode: process.env.NODE_ENV || 'development',
  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    port: 9000,
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader',
    }, {
      test: /\.(css|scss)$/,
      use: [{
        loader: 'style-loader',
      },
      {
        loader: 'css-loader',
        options: {
          minimize: process.env.NODE_ENV === 'production',
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: () => [precss, autoprefixer],
        },
      },
      {
        loader: 'sass-loader',
      }],
    }],
  },
  plugins: [
    new HtmlWebpackPlugin({ template: 'client/index.html' }),
  ],
});
