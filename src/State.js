export default class State {
  feedUrl = '';

  feedTitle = '';

  feedDescription = '';

  feedValidMsg = '';

  isFeedValid = false;

  feadStatus = 'ready';

  feeds = [];

  articles = [];

  changeProp(prop, value) {
    this[prop] = value;
  }

  addArticles(newData) {
    this.articles = [...this.articles, ...newData];
  }

  addFeed(newData) {
    this.feeds = [...this.feeds, newData];
  }
}
