const getInputUrlEl = () => document.querySelector('#rss-url');
const getValidMsgEl = () => document.querySelector('#valid-msg');

export const renderRssValidMsg = (validMsg) => {
  if (!validMsg) {
    getInputUrlEl().classList.remove('is-invalid');
  } else {
    getInputUrlEl().classList.add('is-invalid');
  }
  getValidMsgEl().textContent = validMsg;
};

const getFeedStatusEl = () => document.querySelector('#feed-status');

const renderRssStatus = (msg) => {
  getFeedStatusEl().textContent = msg;
};

const getDownloadBtnEl = () => document.querySelector('#download-btn');

const disableRssInput = (isDisabled) => {
  if (isDisabled) {
    getInputUrlEl().disabled = true;
    getDownloadBtnEl().disabled = true;
  } else {
    getInputUrlEl().disabled = false;
    getDownloadBtnEl().disabled = false;
  }
};

const getFeedDescriptionEl = () => document.querySelector('#feed-description');
const getFeedTitleEl = () => document.querySelector('#feed-title');
const getFeedGroupEL = () => document.querySelector('#add-feed-group');

const renderAddFeedInput = (isDispayed, description, title) => {
  getFeedDescriptionEl().value = description;
  getFeedTitleEl().value = title;
  if (isDispayed) {
    getFeedGroupEL().classList.remove('d-none');
  } else {
    getFeedGroupEL().classList.add('d-none');
  }
};

export const renderFeedStatus = (state) => {
  switch (state.feadStatus) {
    case 'ready': {
      renderRssStatus('');
      renderAddFeedInput(false);
      disableRssInput(false);
      break;
    }
    case 'load': {
      renderRssStatus('loading...');
      disableRssInput(true);
      break;
    }
    case 'complete': {
      getInputUrlEl().value = '';
      renderRssStatus('');
      renderAddFeedInput(true, state.feedDescription, state.feedTitle);
      break;
    }
    default:
    case 'error': {
      renderRssStatus('download error');
      disableRssInput(false);
      break;
    }
  }
};

const getFeedsListEl = () => document.querySelector('#feeds-list');

export const renderFeeds = (feeds) => {
  getFeedsListEl().innerHTML = '';
  feeds.forEach((feed) => {
    const divEl = document.createElement('div');
    const titleNode = document.createElement('h5');
    titleNode.append(feed.title);
    const descrNode = document.createElement('span');
    descrNode.append(feed.description);
    divEl.append(titleNode, descrNode);
    divEl.classList.add('shadow-lg', 'p-3', 'mb-5', 'bg-white', 'rounded');
    getFeedsListEl().append(divEl);
  });
};

const getArticlesListEl = () => document.getElementById('articles-list');

export const renderArticles = (articles) => {
  getArticlesListEl().innerHTML = '';
  articles.forEach((feed) => {
    const divEl = document.createElement('div');
    const titleNode = document.createElement('h5');
    titleNode.append(feed.title);
    const linkEl = document.createElement('a');
    linkEl.setAttribute('href', feed.link);
    linkEl.append(titleNode);
    const btnEl = document.createElement('button');
    btnEl.append('Description');
    btnEl.classList.add('btn', 'btn-outline-info');
    btnEl.dataset.toggle = 'modal';
    btnEl.dataset.target = '#articleModal';
    btnEl.dataset.articleId = feed.id;
    divEl.append(linkEl, btnEl);
    divEl.classList.add('shadow-lg', 'p-3', 'mb-5', 'bg-white', 'rounded');
    getArticlesListEl().append(divEl);
  });
};
