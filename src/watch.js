import { watch } from 'melanke-watchjs';
import {
  renderRssValidMsg,
  renderFeeds,
  renderArticles,
  renderFeedStatus,
} from './view';

export default (state) => {
  watch(state, 'feedUrl', () => {
    renderRssValidMsg(state.feedValidMsg);
  });

  watch(state, 'feadStatus', () => {
    renderFeedStatus(state);
  });

  watch(state, 'feeds', () => {
    renderFeeds(state.feeds);
  });

  watch(state, 'articles', () => {
    renderArticles(state.articles);
  });
};
