import { uniqueId } from 'lodash';

const getArticles = (dom) => {
  const articleNodeList = dom.querySelectorAll('item');
  const articles = Array.from(articleNodeList).map(article => ({
    id: uniqueId(),
    title: article.querySelector('title').textContent,
    link: article.querySelector('link').textContent,
    description: article.querySelector('description').textContent,
    pubDate: article.querySelector('pubDate').textContent,
  }));
  return articles;
};

const getRssProps = (dom) => {
  const titleEl = dom.querySelector('channel > title');
  const title = titleEl ? titleEl.textContent : '';
  const descriptionEl = dom.querySelector('channel > description');
  const description = descriptionEl ? descriptionEl.textContent : '';
  return { title, description };
};

export default (data) => {
  const parser = new DOMParser();
  const dom = parser.parseFromString(data, 'application/xml');
  return {
    articles: getArticles(dom),
    rssProps: getRssProps(dom),
  };
};
