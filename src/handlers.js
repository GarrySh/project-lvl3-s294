import { some, uniqueId } from 'lodash';
import isUrl from 'validator/lib/isURL';
import axios from 'axios';
import parse from './parse';

const getHttpResponse = (url) => {
  const urlWithCorsProxy = `https://cors-anywhere.herokuapp.com/${url}`;
  return axios.get(urlWithCorsProxy);
};

export const downloadBtnHandle = state => (event) => {
  event.preventDefault();
  if (!state.isFeedValid) {
    return;
  }
  state.changeProp('feadStatus', 'load');

  getHttpResponse(state.feedUrl)
    .then((response) => {
      const { rssProps } = parse(response.data);
      state.changeProp('feedTitle', rssProps.title);
      state.changeProp('feedDescription', rssProps.description);
      state.changeProp('feadStatus', 'complete');
      return rssProps;
    })
    .catch((error) => {
      state.changeProp('feadStatus', 'error');
      state.changeProp('isFeedValid', false);
      console.error('error load feeds', error);
    });
};

export const inputUrlHandle = state => (event) => {
  const { value } = event.target;
  state.changeProp('feedUrl', value);
  state.changeProp('feadStatus', 'ready');
  if (value === '') {
    state.changeProp('feedValidMsg', '');
    state.changeProp('isFeedValid', false);
  } else if (!isUrl(value)) {
    state.changeProp('feedValidMsg', 'invalid url format');
    state.changeProp('isFeedValid', false);
  } else if (some(state.feeds, { url: value })) {
    state.changeProp('feedValidMsg', 'url already in use');
    state.changeProp('isFeedValid', false);
  } else {
    state.changeProp('feedValidMsg', '');
    state.changeProp('isFeedValid', true);
  }
};

export const feedDescriptionHandle = state => (event) => {
  const { value } = event.target;
  state.changeProp('feedDescription', value);
};

export const feedTitleHandle = state => (event) => {
  const { value } = event.target;
  state.changeProp('feedTitle', value);
};

export const addFeedBtnHandle = state => (event) => {
  event.preventDefault();
  state.addFeed({
    id: uniqueId(),
    url: state.feedUrl,
    title: state.feedTitle,
    description: state.feedDescription,
  });
  getHttpResponse(state.feedUrl)
    .then((response) => {
      const { articles } = parse(response.data);
      state.addArticles(articles);
      return articles;
    })
    .catch((error) => {
      console.error('error load articles', error);
    });
  state.changeProp('feedUrl', '');
  state.changeProp('feadStatus', 'ready');
};

export const cancelBtnHandle = state => (event) => {
  event.preventDefault();
  state.changeProp('feedUrl', '');
  state.changeProp('feadStatus', 'ready');
};
