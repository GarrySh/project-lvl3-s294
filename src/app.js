import $ from 'jquery';
import 'bootstrap';
import { find } from 'lodash';
import addWatch from './watch';
import State from './State';
import {
  downloadBtnHandle,
  inputUrlHandle,
  feedDescriptionHandle,
  feedTitleHandle,
  addFeedBtnHandle,
  cancelBtnHandle,
} from './handlers';

export default () => {
  const state = new State();

  addWatch(state);

  document.getElementById('download-btn').addEventListener('click', downloadBtnHandle(state));
  document.getElementById('rss-url').addEventListener('input', inputUrlHandle(state));
  document.getElementById('feed-description').addEventListener('input', feedDescriptionHandle(state));
  document.getElementById('feed-title').addEventListener('input', feedTitleHandle(state));
  document.getElementById('add-feed-btn').addEventListener('click', addFeedBtnHandle(state));
  document.getElementById('cancel-btn').addEventListener('click', cancelBtnHandle(state));

  $('#articleModal').on('show.bs.modal', function showModal(event) {
    const button = $(event.relatedTarget);
    const articleId = button.data('articleId').toString();
    const article = (find(state.articles, { id: articleId }));
    const modal = $(this);
    const modalBody = modal.find('.modal-body');
    modal.find(modalBody).text(article.description);
  });
};
