# RSS Reader 

[![Build Status](https://travis-ci.com/GarrySh/project-lvl3-s294.svg?branch=master)](https://travis-ci.com/GarrySh/project-lvl3-s294)
[![Maintainability](https://api.codeclimate.com/v1/badges/f08cc5187b0c2fb99786/maintainability)](https://codeclimate.com/github/GarrySh/project-lvl3-s294/maintainability)
[![Test Coverage](https://api.codeclimate.com/v1/badges/f08cc5187b0c2fb99786/test_coverage)](https://codeclimate.com/github/GarrySh/project-lvl3-s294/test_coverage)

Online demo: http://rss-reader-garrysh.surge.sh

## Requirements

* Node.JS
* Yarn
